%!TeX root=../TextoGabriely.tex
%("dica" para o editor de texto: este arquivo é parte de um documento maior)
% para saber mais: https://tex.stackexchange.com/q/78101/183146

%% ------------------------------------------------------------------------- %%
\chapter{Fundamentos}
\label{cap:fundamentos}
Veremos agora alguns conceitos e técnicas abordadas ao longo desta pesquisa para um melhor entendimento da proposta que este trabalho apresenta e dos resultados obtidos. %Este capítulo tem o objetivo de apresentar simplificadamente as técnicas computacionais utilizadas para alcançar o objetivo deste trabalho. Como não iremos nos aprofundar demasiadamente nesses conceitos, algumas referências estarão associadas ao texto para o leitor que preferir conhecê-los em maiores detalhes.
Dessa forma, separamos este conteúdo inicial em três seções distintas. 
Na primeira parte veremos quais são as técnicas de \textbf{Sistemas Complexos} utilizadas para analisar o processo de regionalização de saúde, na Seção \ref{sec:sistemas_complexos}. 
Na segunda parte observaremos o que são os \textbf{Modelos de Fluxo de Oferta e Demanda} e como eles foram utilizados para analisar e avaliar a autossuficiência das regiões de saúde, a Seção \ref{sec:modelagem_de_fluxos}. 
Por fim, veremos alguns \textbf{Outros conceitos} utilizados aqui de forma secundária para auxiliar as abordagens principais, na Seção \ref{sec:outros_conceitos}. 

%% ------------------------------------------------------------------------- %%
\section{Sistemas Complexos}
\label{sec:sistemas_complexos}
Sistemas complexos estão presentes de forma abundante no mundo real. Estes são alguns exemplos em que encontramos o comportamento de um sistema complexo: no funcionamento dos neurônios no cérebro humano, numa colônia de formigas, no tráfego de veículos em uma rodovia, na coleção dos artigos científicos já publicados, na World Wide Web (WWW), nas redes sociais, entre outros casos. Observamos uma característica em comum entre eles: cada um de seus elementos não age através de uma visão geral do sistema, mas o sistema se mantém e evolui através das ações individuais de cada elemento, sendo que eles possuem um conhecimento apenas local. Embora não possua uma coordenação geral, como um líder, o sistema complexo apresenta uma estrutura organizada, com resultados interessantes e até surgimento de padrões inesperados em alguns casos \citep{barrat, boccara}.
Por exemplo, \cite{zhong2014detecting} apresentam uma aplicação atual de análise por sistemas complexos. 
O trabalho estuda a dinâmica da estrutura urbana, a partir da movimentação de pessoas, 
para identificar a estrutura espacial das cidades, elemento importante para entender as interações urbanas. 

Não há uma definição única e comumente aceita de complexidade. Entretanto, um ponto que geralmente caracteriza um sistema complexo é sua \textbf{propriedade emergente} \citep{barrat}. Embora não seja a única, a propriedade de emergência é a principal em um sistema complexo. Um sistema considerado complexo é geralmente composto por um conjunto de elementos que conseguem interagir entre si localmente de forma que produzem um fenômeno emergente, algum padrão ou efeito observado em larga escala. Essa propriedade é definida pelos efeitos vistos em larga escala de interações locais entre os elementos do sistema \citep{boccara}. Dessa forma, observamos que em sistemas complexos o todo é mais que a soma de suas partes. Isto é, o comportamento geral do sistema possui características que vão além das que são observadas no comportamento individual dos seus elementos \citep{mitchell, barrat}. 

Em uma rede social, por exemplo, existe uma probabilidade maior de um indivíduo se conectar a indivíduos mais populares do que a indivíduos mais isolados. Especificando dessa forma o comportamento local de elementos em uma rede social, outro comportamento geral, não especificado, pode ser observado. Sua estrutura apresenta um padrão próprio, claramente diferente da estrutura de uma rede de conexões aleatórias, por exemplo. Esse padrão emergente se forma na rede mesmo que seus elementos não tenham a intenção e talvez nem o conhecimento de que esse efeito esteja sendo por eles produzido.

A área de sistemas complexos conta com um conjunto de técnicas e conceitos desenvolvidos inicialmente em outras áreas como estatística, computação, física e geografia. Isso engloba modelagens, correlações, simulações, algoritmos e conceitos de comportamento caótico, dinâmica não-linear e teoria de grafos \citep{amaral}. Além disso, suas aplicações também são observadas em diversas áreas do conhecimento em sistemas biológicos, físicos, econômicos, sociais, epidemiológicos, tecnológicos, entre outros.

%% ------------------------------------------------------------------------- %%
\subsection{Redes Complexas}
\label{subsec:redes_complexas}
Um grafo é composto por um conjunto finito de vértices, um conjunto finito de arestas e uma regra que defina quais vértices são conectados pelas arestas \citep{graphtheory}. 
As arestas têm a função de conectar dois vértices de forma direcionada ou não direcionada. Cada vértice e cada aresta pode possuir propriedades individuais, como um nome e um peso específico. 
A origem da teoria de grafos remonta ao século XVIII, quando Leonhard Euler (1707–1783) demonstra que o problema das pontes de Königsberg não tem solução. 
Uma rede complexa é a representação de um sistema complexo através de conceitos da teoria de grafos \citep{strogatz}. Uma rede complexa representa a relação dos elementos — os vértices — entre si. 

Neste projeto, por exemplo, uma rede complexa é criada para modelar o fluxo intermunicipal de internações hospitalares atendidas pelo SUS nas últimas décadas. 
Em nosso caso, os vértices são municípios de um mesmo estado brasileiro e as arestas indicam a movimentação dos pacientes do município de residência ao município de atendimento. 
Em alguns momentos as análises mudam de nível, de municipal para regional. Nesses momentos os vértices são regiões ou macrorregiões de saúde. 
De toda forma, todas as análises buscam investigar o comportamento ou eficiência da regionalização da saúde nos estados estudados.

Outras áreas, além da saúde pública, aplicam conceitos de redes complexas para modelar o comportamento de sistemas biológicos, sociais, físicos, entre outros \citep{barrat}. 
O conceito de complexidade em redes, assim como em sistemas complexos, também pode ser caracterizado pela presença de algum comportamento emergente, como explicado anteriormente. 
Veremos a seguir um possível comportamento emergente em redes complexas. Ele é colocado em evidência por uma técnica aplicada na rede visando identificar a tendência de agrupamento entre os elementos da rede, a detecção de comunidades.

%% ------------------------------------------------------------------------- %%
\subsubsection{Detecção de comunidades}
\label{subsubsec:deteccao_de_comunidades}
O objetivo das técnicas de detecção de comunidades é tentar inferir relações entre vértices da rede que não podem ser mensuradas diretamente por observação \citep{fortunato}. Essas técnicas analisam a distribuição/concentração de arestas entre os vértices através de métricas de centralidade, distância ou similaridade, por exemplo, e fornecem os possíveis agrupamentos encontrados. 
Dessa forma, cada comunidade detectada é um subgrafo do grafo definido pela rede original, e a união de todos os subgrafos contém todos os vértices da rede original. 
A Figura \ref{fig:comunidades_exemplo} exemplifica esse conceito. 

\begin{figure}
  \centering
    \includegraphics[width=0.9\textwidth]{comunidades}
  \caption{Uma rede de 23 vértices, de estrutura modularizada, particionada em três comunidades, representadas por cores diferentes.}
  \label{fig:comunidades_exemplo}
\end{figure}

Esse procedimento pode ser realizado por algoritmos de categorias diferentes. 
A escolha de um bom algoritmo para cada caso é uma tarefa que demanda um conhecimento mais amplo da literatura, pois as diferentes categorias de algoritmos consideram aspectos diferentes da rede. 
Neste projeto, foram testados dois algoritmos: o \textit{greedy modularity} \citep{girvan2002,newman2004,newman2004fast,clauset2004} e o \textit{infomap} \citep{rosvall2008,rosvall2009,rosvall2011}.

\pagebreak
%% ------------------------------------------------------------------------- %%
\paragraph{\textit{Greedy modularity}}
\label{paragraph:greedy_modularity}
É um dos mais conhecidos algoritmos que utilizam o método divisivo. Esse método identifica as arestas que interconectam comunidades diferentes e as removem da rede, de forma que as comunidades terminam desconectadas umas das outras. 
Para isso, se utiliza a métrica de \textit{centralidade de intermediação}, definida como:
\[ B_{e}=\sum^{}_{i,j}\frac{\sigma (i,e,j)}{\sigma (i,j)}, \]
onde $\sigma (i,e,j)$ é a quantidade de caminhos mínimos entre os vértices \textit{i} e \textit{j} que passam pela aresta \textit{e}; 
$\sigma (i,j)$ é a quantidade total de caminhos mínimos entre os vértices \textit{i} e \textit{j}; e 
a soma passa por todos os pares de vértices \textit{i} e \textit{j} distintos \citep{costa2007}. 
Para identificar essas arestas que interconectam as comunidades, o algoritmo calcula a \textit{centralidade de intermediação} para cada aresta e remove a que tiver o maior valor. Faz isso repetidamente criando as possíveis divisões da rede e escolhe a divisão de comunidades ideal através de um critério de parada, como apresentado na Figura \ref{fig:modularity_figure}. O valor da \textit{centralidade de intermediação} para uma aresta é determinado pela quantidade de vezes em que a aresta é utilizada em todos os caminhos mínimos possíveis da rede. 
Dessa forma, as arestas removidas são as mais utilizadas para conectar quaisquer dois vértices na rede \citep{fortunato}.

\begin{figure}
  \centering
    \includegraphics[width=\textwidth]{modularity_figure}
  \caption{Representação de uma rede de 64 vértices em dendrograma com o gráfico da métrica \textit{modularity} para cada partição possível utilizando o método descrito no texto. A linha vermelha tracejada indica a partição onde se obtém o maior valor para a métrica nesta rede. As 4 diferentes comunidades particionadas são representadas por 4 formatos de vértices diferentes no dendrograma \citep{newman2004}.}
  \label{fig:modularity_figure}
\end{figure}

O critério de parada utilizado para este algoritmo é a maximização da métrica \textit{modularity}. Ela valoriza as divisões que possuem uma maior quantidade de arestas que não cruzam comunidades diferentes, isto é, arestas com vértices na mesma comunidade. Mas isso não é o suficiente, pois, por exemplo, uma única comunidade abrangendo toda a rede ocasionaria no maior número de arestas da mesma comunidade embora não seja a melhor divisão na maioria dos casos. Portanto, a métrica \textit{modularity} é composta pela quantidade de arestas cujas extremidades pertencem à mesma comunidade na rede, menos a quantidade esperada dessas arestas em uma rede aleatória. Logo, a métrica será igual a zero quando a quantidade de arestas da mesma comunidade não é melhor que na versão aleatória; e a métrica cresce indicando uma melhor divisão \citep{newman2004}. Uma otimização gulosa (\textit{greedy}) para a métrica \textit{modularity} é proposta pelos algoritmos apresentados por \cite{newman2004fast} e \cite{clauset2004}, sendo este último o que é utilizado no presente projeto.

%% ------------------------------------------------------------------------- %%
\paragraph{\textit{Infomap}}
\label{paragraph:infomap}
Esse algoritmo considera que a rede representa um sistema com interações locais carregando informações que fluem através de arestas direcionadas e com pesos, algo particularmente fundamental para este trabalho. 
Para isso, o \textit{infomap} utiliza passeio aleatório (\textit{random walk}) como um \textit{proxy} para medir a difusão de informação na rede. 
Um passeio aleatório é feito considerando as direções das arestas e seus pesos, essas informações influenciam as decisões do passeio de forma que afetam a frequência em que cada vértice é visitado — semelhante ao que acontece no fluxo real da rede \citep{rosvall2009}. 
Por meio do passeio, cada vértice é rotulado utilizando um método de compressão de informação — a codificação de Huffman \citep{huffman} — com base na frequência que o vértice é visitado pelo passeio. 
Para ilustrar o funcionamento da codificação de Huffman, vejamos um exemplo de codificação de um passeio de 7 passos numa rede de 5 vértices, como apresenta a Figura \ref{fig:huffman_ex_net}. 

\begin{figure}
  \centering
    \includegraphics[width=0.4\textwidth]{huffman_ex_net}
  \caption{Exemplo de um passeio de 7 passos numa rede de 5 vértices, iniciando pelo vértice $A$.}
  \label{fig:huffman_ex_net}
\end{figure}

O passeio pode ser descrito pela sequência de vértices visitados, nesse caso: $ABCDBCDE$. A menor codificação que podemos utilizar para representá-los em binário é de três bits por caractere, da seguinte forma: 
A = 000; B = 001; C = 010; D = 011; E = 100. 
Sendo assim, a sequência se transforma em $000 001 010 011 001 010 011 100$, um total de 24 bits. 
A codificação de Huffman tem como base a frequência em que cada vértice é visitado. 
Isto é, A é visitado 1 vez, B 2 vezes, C 2 vezes, D 2 vezes e E 1 vez. 
Com isso, o algoritmo monta uma árvore binária, a árvore de Huffman, unindo os caracteres menos frequentes dois a dois, gerando novos nós, e adicionando os valores 0 e 1 para cada par de arestas partindo de algum nó. 
A Figura \ref{fig:huffman_ex_tree} apresenta a árvore de Huffman para este exemplo. 

\begin{figure}
  \centering
    \includegraphics[width=0.6\textwidth]{huffman_ex_tree}
  \caption{Árvore de Huffman para a compressão da sequência $ABCDBCDE$.}
  \label{fig:huffman_ex_tree}
\end{figure}

Podemos identificar os novos códigos para cada caractere percorrendo a árvore de Huffman desde a raiz até a folha do caractere correspondente guardando os valores de cada aresta percorrida (em azul na árvore). 
Agora, os caracteres mais frequentes possuem uma codificação menor, enquanto os menos frequentes podem ter uma codificação maior.
Dessa forma, podemos chegar a uma codificação comprimida para os caracteres, sendo ela: 
A = 000; B = 01; C = 10; D = 11; E = 001. Sendo assim, a sequência comprimida é $000 01 10 11 01 10 11 001$, somando 18 bits, economizando 6 bits em relação à sequência original. 

Voltando para as comunidades, a Figura \ref{fig:infomap_figure}a mostra o trajeto de um possível passeio aleatório numa rede de estrutura modularizada e a Figura \ref{fig:infomap_figure}b apresenta a aplicação da codificação de Huffman para esse passeio como primeira etapa para detectar as comunidades. 
Portanto, os vértices mais visitados são rotulados com códigos menores (com menos bits), com a intenção de minimizar o comprimento esperado da descrição de um passeio aleatório, isto é, compactar a sequência com os códigos dos vértices visitados.

\begin{figure}
  \centering
    \includegraphics[width=\textwidth]{infomap_figure}
  \caption{Processo de detecção de comunidades através da compressão da descrição do trajeto do passeio aleatório. a) Trajeto de um passeio aleatório na rede. b) Rotulagem dos vértices pela codificação de Huffman. c) Descrição do passeio aleatório em dois níveis \citep{rosvall2008}.}
  \label{fig:infomap_figure}
\end{figure}

Para decompor a rede em comunidades, como mostra a Figura \ref{fig:infomap_figure}c, o algoritmo dá um passo a mais na compressão da informação: ele considera outro nível de relacionamento entre os vértices analisando a frequência que o passeio aleatório permanece em certos grupos, nomeando-os conforme a frequência em que são acessados. Como explicado por \cite{rosvall2008}, os rótulos obtidos pelo passeio são compactados de forma semelhante ao que é feito produzindo um mapa (daqui vem o nome \textit{infomap}): o cartógrafo precisa balancear as informações que estarão presentes no mapa entre informações mais e menos relevantes, dando maior ênfase às estruturas mais relevantes e frequentes, e menor ênfase às outras, porque o espaço do mapa é limitado. Por exemplo, consideremos cada cidade do mapa como uma comunidade e o conjunto de ruas como seus vértices. Sabemos que duas ruas em duas cidades diferentes podem ter nomes iguais sem causar muita confusão, pois temos a informação da cidade a qual cada uma pertence. Semelhantemente, a rede é dividida em dois níveis de descrição: comunidades e vértices. Uma codificação única de Huffman é gerada para nomear as comunidades (as estruturas mais relevantes) e outras codificações são geradas para cada comunidade sem a necessidade de unicidade, pois não causam confusão ao pertencerem a comunidades diferentes. 
A compressão da codificação nessa forma é mais eficiente do que a codificação em apenas um nível — o nível dos vértices, como visto anteriormente. 
Diferente dos algoritmos que otimizam a métrica \textit{modularity}, o \textit{infomap} otimiza a métrica \textit{map equation}, que por sua vez valoriza o fluxo e a dinamicidade da rede para compreender melhor a sua estrutura \citep{rosvall2008}.
Para isso, o \textit{infomap} busca minimizar a \textit{map equation} definida, resumidamente, como:
\begin{align}
  L(M) & = q H(Q) + \sum^{m}_{i=1} p_{i} H(P_{i}) \nonumber
\end{align} 
onde $L(M)$ é a quantidade média de bits utilizada por passo no passeio aleatório (o valor a ser minimizado); 
$M$ é a partição atual da rede entre $m$ módulos/comunidades; 
$q$ é a probabilidade do passo estar atravessando comunidades; 
$H(Q)$ é a quantidade média de bits utilizada para movimentos entre comunidades; 
$p_{i}$ é a probabilidade do passo estar na comunidade $i$; 
e $H(P_{i})$ é a quantidade média de bits utilizada para movimentos na comunidade $i$. 

Logo, $q H(Q)$ é a quantidade média de bits necessária para descrever movimentos que atravessam comunidades 
e $\sum^{m}_{i=1} p_{i} H(P_{i})$ representa a quantidade média de bits necessária para descrever movimentos nas comunidades. 

Dessa forma, o algoritmo \textit{infomap} para detecção de comunidades é equivalente ao problema de compressão de informação \citep{rosvall2007}. Seu objetivo é minimizar a informação (quantidade de bits) necessária para descrever cada passo do passeio aleatório na rede utilizando dois níveis de descrição, gerando os agrupamentos.

Passando para o contexto analisado neste trabalho, a rede utilizada para gerar agrupamentos é a rede de fluxos intermunicipais de pacientes. 
Neste caso, o algoritmo de detecção de comunidades gera agrupamentos de municípios. Como as regiões de saúde também são agrupamentos de municípios, 
faremos, por fim, uma análise comparativa dos dois resultados analisando sua similaridade. 

%% ------------------------------------------------------------------------- %%
\section{Modelagem de fluxos de oferta e demanda}
\label{sec:modelagem_de_fluxos}
Outro conceito utilizado neste trabalho é a modelagem de fluxos de oferta e demanda. Um fluxo baseado em demanda é definido como um tráfego fluindo entre a origem e o destino dentro de um sistema que interage como um todo \citep{hodgson}. 
Esse conceito é utilizado no contexto de áreas de influência de hospitais para oferta de serviços hospitalares e demanda de pacientes com necessidade de internação. 
Em nosso caso, as áreas de influência são as regiões de saúde, de forma que a análise do fluxo de internação entre regiões apresente resultados interessantes para uma avaliação de suficiência dessas mesmas regiões.

%% ------------------------------------------------------------------------- %%
\subsection{Interação Espacial}
\label{subsec:interacao_espacial}
A modelagem de sistemas complexos se refere ao estudo da dinâmica da interação entre os elementos. Para isso, tem-se utilizado matemática não-linear, simulações computacionais e visualização de dados \citep{wilson}. A interação espacial se apresenta essencialmente como fluxos de elementos em um sistema \citep{hayes}. Ela é um processo que permite a análise do comportamento do sistema em que entidades em diferentes pontos de um espaço físico têm contato, tomam decisões de demanda e oferta, ou realizam escolhas regionais \citep{oliveira}. Os modelos de interação espacial seguem o conceito do modelo gravitacional. A lei gravitacional de varejo, introduzida por Reilly, motivou a criação dos modelos gravitacionais. Essa lei afirma que o cliente está disposto a percorrer uma longa distância para lojas maiores, quanto maior for a atração que elas apresentam aos clientes \citep{reilly}. Neste caso, a lei pode ser facilmente aplicada na questão do deslocamento de pacientes para hospitais, sendo o nível de atração relacionado a disponibilidade e qualidade dos recursos do hospital. O termo “gravitacional” se refere à lei da gravidade na Física, que se comporta de forma semelhante ao trocar a atratividade pela massa. Esse tipo de modelo tem como característica principal a modelagem do comportamento de segmentos de oferta e demanda \citep{roy}. O fluxo intermunicipal de pacientes para atendimento hospitalar oferece uma estrutura de oferta e demanda dos serviços de saúde. Em estudos de mercados e de áreas de influência hospitalar os coeficientes \textit{Little In From Outside} (LIFO) e \textit{Little Out From Inside} (LOFI) \citep{elzinga1973, elzinga1978} já foram utilizados anteriormente \citep{rigoli2019,frech}.


%% ------------------------------------------------------------------------- %%
\subsubsection{Coeficientes LIFO e LOFI}
\label{subsubsec:coeficientes}
O método desenvolvido por Elzinga e Hogarty em 1973 busca resolver o problema de delimitação da área do mercado geográfico. Conforme a Enciclopédia Jurídica da PUCSP:

\begin{quote}
  \textit{Fala-se de mercado relevante geográfico enquanto uma determinada área na qual empresas atuam na oferta e demanda dos produtos ou serviços, em condições de concorrência suficientemente homogêneas e claramente distinguíveis em suas diferenças das áreas vizinhas. }\citep{enciclopediajuridica}
\end{quote}

Essa área compreende o espaço onde as empresas atuam em certa concorrência de forma que a área seja relativamente \textbf{autocontida} (ou \textbf{autossuficiente}) \citep{frech}. Baseado nessa ideia, os coeficientes foram criados para definir o nível de suficiência de uma determinada área através de duas medidas.

\begin{figure}[H]
  \centering
    \includegraphics[width=\textwidth]{lifo_lofi_figure}
  \caption{Diagrama do fluxo de pacientes em certa região de saúde e sua representação pelos coeficientes de Elzinga e Hogarty.}
  \label{fig:lifo_lofi_figure}
\end{figure}

No contexto de atendimento hospitalar, o coeficiente LIFO (\textit{Little In From Outside}) indica que os hospitais atendem poucos pacientes de fora da região de saúde em que está localizado. 
Ele mede a proporção dos pacientes que residem e são atendidos nesta região, dentre todos os atendimentos hospitalares que essa mesma região realizou. 
Dessa forma, a medida será inversamente proporcional à quantidade de pacientes que residem fora da região. 
Já o coeficiente LOFI (\textit{Little Out From Inside}) indica que poucos pacientes da região são atendidos fora dela. 
Especificamente, ele mede a proporção dos pacientes que residem e são atendidos nesta região, dentre todos os pacientes que residem nesta região e são atendidos dentro ou fora dela. 
Isto é, ele é inversamente proporcional à quantidade de pacientes que buscam atendimento fora de sua região. 
Os coeficientes são valores entre 0 e 1 calculados da seguinte forma:
\begin{align*}
  \text{LIFO}& = \frac{\text{atendimentos de dentro da própria região}}{\text{atendimentos de dentro + recebidos de fora da região}};
  &\\
  &\\
  \text{LOFI}& = \frac{\text{atendimentos de dentro da própria região}}{\text{atendimentos de dentro + mandados para fora da região}}.
\end{align*}
A Figura \ref{fig:lifo_lofi_figure} apresenta o cálculo dessas medidas em relação ao fluxo de pacientes em certa região de saúde. 

%Como visto anteriormente, ambas as medidas são inversamente proporcionais à quantidade de pacientes que buscam atendimento fora da sua região de origem. 
%Sendo assim, quanto maior for cada coeficiente para uma determinada região, mais autossuficiente ela será \citep{frech}. 
%Elzinga e Hogarty sugerem para ambos coeficientes uma porcentagem mínima de 75\% para definir um mercado relevante, porém um “mercado relevante fraco”, e uma porcentagem mínima de 90\% para determinar um “mercado relevante forte”. 
No contexto de regiões de saúde, a combinação desses dois coeficientes pode determinar o nível de suficiência dessas regiões. 
Veremos com mais detalhes na Seção \ref{subsec:metodo_indice} como isso foi utilizado para analisar e avaliar a delimitação das regiões de saúde no Brasil.

%% ------------------------------------------------------------------------- %%
\section{Outros conceitos}
\label{sec:outros_conceitos}
Para aplicar as técnicas apresentadas nas seções anteriores, a fim de responder às questões de pesquisa deste projeto, fizemos uso de alguns outros conceitos que aparecem aqui de forma secundária. 
Esses conceitos, juntamente com as técnicas principais, são essenciais para que a pesquisa alcance o objetivo final. 
Vejamos a seguir esses conceitos secundários, a métrica de similaridade entre partições e a média harmônica ponderada. 

%% ------------------------------------------------------------------------- %%
\subsection{Similaridade entre partições}
\label{subsec:similaridade_particoes}
As métricas de similaridade entre partições avaliam o quão semelhante são duas partições de um mesmo conjunto de elementos. 
No contexto de redes complexas, uma partição $M$ de uma rede a separa em $m$ módulos/comunidades diferentes, agrupando os vértices em comunidades. 
Por exemplo, na Figura \ref{fig:comunidades_exemplo} a rede apresentada está particionada em três módulos diferentes, cada um representado por uma cor. 
Para calcular o nível de similaridade de duas partições, elas precisam ser partições de um mesmo conjunto de elementos e devem ter a mesma quantidade de módulos. 

A principal aplicação dessa métrica se encontra na avaliação do desempenho de algoritmos de clusterização, 
algoritmos de aprendizado não supervisionado que geram agrupamentos (\textit{clusters}) em um conjunto de dados \citep{jain1988}. 
Nesse cenário, a métrica de similaridade de partições compara a partição obtida pelo algoritmo de clusterização, com a partição real dos dados em classes diferentes. 
Logo, o nível de similaridade indica o nível de eficiência da clusterização em ambientes de treinamento e teste. 

Vários algoritmos já foram desenvolvidos para calcular essa métrica, como o \textit{V-measure \citep{vmeasure}, Adjusted Rand index \citep{hubert1985comparing, steinley2004properties}, Fowlkes-Mallows score \citep{fowlkes1983method}}, entre outros. 
Seu valor geralmente varia entre 0 e 1, onde um alto valor indica uma maior semelhança entre as partições. 
Em nosso contexto de análise das regiões de saúde, essa métrica é utilizada na rede de movimentação de pacientes entre municípios de um estado específico. 
O algoritmo compara duas partições da mesma rede: a divisão original dos municípios em regiões de saúde e a divisão sugerida pelo método de detecção de comunidades, apresentado anteriormente (vide Seção \ref{subsubsec:deteccao_de_comunidades}). 
Com isso, conseguimos comparar numericamente a semelhança entre as comunidades sugeridas pelo algoritmo e as regiões de saúde. 

Assim como será explicado na Seção \ref{subsec:semelhanca_entre_particoes}, após testarmos diferentes algoritmos de comparação de partições, optamos por utilizar o \textit{Adjusted Rand Index}. 
Esse algoritmo realiza a comparação dos resultados de acordo com a frequência em que cada par de elementos é classificado da mesma forma. 
Isto é, sendo $M$ o conjunto de $m$ municípios de um estado brasileiro, $R$ é a partição dos municípios em $n$ regiões de saúde e $S$ a partição em $n$ agrupamentos sugeridos. 
Para todo par $m_i, m_j$ de municípios em $M$ (para $i \neq j$), calcula-se a frequência em que $m_i$ e $m_j$ estão ambos no mesmo agrupamento para $R$ e $S$, ou estão ambos classificados em agrupamentos diferentes em $R$ e $S$. 
Esse valor em relação a todos os pares possíveis $m \choose 2$ é o \textit{Rand Index}. 
Por fim, para se obter o \textit{Adjusted Rand Index}, é feita uma correção para que os casos de classificações aleatórias não gerem um índice alto. 
Isso é obtido ao se estabelecer um modelo aleatório como ponto de partida, de forma que o valor final seja próximo de zero quando a classificação se aproxima da aleatória. 

%% ------------------------------------------------------------------------- %%
\subsection{Média harmônica ponderada}
\label{subsec:media_harmonica}
Na matemática, existem diferentes formas de se calcular a média de um conjunto de valores. 
Entre as mais conhecidas estão a \textit{média aritmética}, a \textit{média geométrica} e a \textit{média harmônica}. 
A média aritmética é a mais utilizada no nosso dia-a-dia para situações simples, 
a média geométrica é geralmente utilizada para comparar valores de escalas diferentes, e
a média harmônica é indicada para calcular a média de duas ou mais taxas (valores entre 0 e 1). 

O cálculo da média harmônica de um conjunto de elementos $x_{1}, x_{2}, ..., x_{n} > 0$ é definida pela equação:
\[ \bar{h}=\frac{n}{\sum^{n}_{i=1}\frac{1}{x_{i}}}, \]
Para estabelecer pesos diferentes $w_{i}$ para cada valor $x_{i}$, calculamos a média harmônica ponderada, definida como: 
\[ \bar{h}(w)=\frac{\sum^{n}_{i=1}w_{i}}{\sum^{n}_{i=1}\frac{w_{i}}{x_{i}}}. \]

A média harmônica é aplicada em diferentes áreas do conhecimento. Por exemplo, na Física, ao calcular a média de velocidades, em Finanças, na relação de preço e ganho, na Ciência da Computação, em aprendizado de máquina e recuperação de informação (com o algoritmo \textit{F-score}), entre outras áreas. 
No contexto deste trabalho, a média harmônica ponderada é utilizada para ponderar os dois coeficientes LIFO e LOFI modificados (vide \ref{subsubsec:coeficientes}), para gerar um indicador final para o desempenho das regiões de saúde, conforme a movimentação de pacientes entre as regiões. 
No Capítulo \ref{cap:metodologia} observaremos como os dois coeficientes serão ponderados para avaliar o desempenho da regionalização dos estados brasileiros. 

Antes de apresentarmos a metodologia desenvolvida nesta pesquisa, mostraremos no próximo capítulo como os principais conceitos descritos aqui são abordados pela literatura. 
O Capítulo \ref{cap:trabalhos} apresenta os trabalhos relacionados a este e faz uma breve comparação deles com o que desenvolvemos nesta dissertação. 
