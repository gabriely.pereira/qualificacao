%!TeX root=../TextoGabriely.tex
%("dica" para o editor de texto: este arquivo é parte de um documento maior)
% para saber mais: https://tex.stackexchange.com/q/78101/183146

%% ------------------------------------------------------------------------- %%
\chapter{Metodologia}
\label{cap:metodologia}
Neste capítulo, introduzimos as bases de dados utilizadas e os métodos de análise de cada abordagem em resposta às questões de pesquisa. 
Por fim, apresentamos o plano de desenvolvimento da plataforma web do tipo \textit{dashboard} que disponibiliza informações sobre o 
processo de regionalização do Sistema Único de Saúde (SUS) em suporte a gestores de saúde e pesquisadores.
Para isso, o capítulo foi dividido em três seções: a Seção \ref{sec:bases} apresenta as bases de dados 
utilizadas para permitir a aplicação dos métodos de análise escolhidos; a Seção \ref{sec:abordagens} 
apresenta as abordagens utilizadas para analisar o processo de regionalização; e a Seção 
\ref{sec:plataforma} descreve detalhes de implementação da plataforma, que foi criada para conter todas as visualizações geradas durante as análises, 
para dar apoio à elaboração de políticas públicas.


%% ------------------------------------------------------------------------- %%
\section{Bases de Dados}
\label{sec:bases}
Para analisar as regiões de saúde através de técnicas de sistemas complexos, utilizamos dados de deslocamento 
intermunicipal dos pacientes. As informações de origem e destino do paciente internado podem ser obtidas 
através do Sistema de Informações Hospitalares do SUS (SIHSUS), a base de internações hospitalares. Por meio dela 
conseguimos representar a rede de deslocamento dos pacientes entre os municípios. Para visualização das análises e 
descrição estatística da base, utilizamos também a Malha Territorial ao nível de município e o Censo Demográfico de 2010 com as estimativas populacionais, fornecidos pelo Instituto 
Brasileiro de Geografia e Estatística (IBGE), e os limites das regiões e macrorregiões de saúde, do Departamento de Monitoramento e Avaliação do SUS (DEMAS). 
Com isso, conseguimos apresentar as análises através de visualizações
georreferenciadas com os limites municipais e regionais.

%% ------------------------------------------------------------------------- %%
\subsection{Internações Hospitalares}
\label{subsec:sih}
Os dados relacionados às internações hospitalares foram obtidos pelo SIHSUS. 
Esse é um dos sistemas desenvolvidos pelo Departamento de Informática do SUS (DATASUS), que tem o objetivo 
de promover soluções tecnológicas para melhorar o acesso e a organização de dados do SUS. 
Os dados fornecidos pelo SIHSUS são estruturados de forma a conter dados pessoais do paciente, da internação e do 
estabelecimento em que foi atendido. Entre esses dados estão: município de origem do paciente, faixa etária, nível de 
escolaridade, diagnóstico principal (conforme a Classificação Internacional de Doenças, a CID-10), 
nível de complexidade do procedimento, duração da 
internação, localização do estabelecimento, entre outras variáveis.

Nosso projeto analisa a rede de movimentação dos pacientes pelos municípios ao longo das últimas duas décadas. 
Para isso, utilizamos a soma de internações para cada origem e destino da base. Isto é, dentre as 
variáveis fornecidas pelo SIHSUS, utilizamos o município de residência do paciente e o município do estabelecimento 
que o atende, para cada ano da base. Isso é o suficiente para a criação da rede de movimentação de pacientes, permitindo 
a aplicação das técnicas de sistemas complexos (Seção \ref{sec:sistemas_complexos}) e de modelagem de fluxos de oferta e 
demanda (Seção \ref{sec:modelagem_de_fluxos}) para analisar o processo de regionalização do SUS.

Além disso, podemos utilizar as mesmas ferramentas aplicadas neste trabalho para analisar redes mais específicas de movimentação de 
pacientes, filtrando internações por variáveis da base de dados. Se filtrarmos a base pela variável que representa o 
diagnóstico principal da internação, por exemplo, podemos observar o comportamento da rede para um agravo específico 
(como doenças do aparelho circulatório) e analisar todo o comportamento e eficiência das regiões de saúde para este caso específico. 
A Figura \ref{fig:fluxo_diagnosticos} apresenta dois diagnósticos distintos para o ano de 2019, mostrando como a mobilidade de pacientes difere para ambos. 
O mesmo processo pode ser realizado facilmente, em trabalhos futuros, com o uso de outras variáveis da base para analisar o comportamento da rede em outras perspectivas.

\begin{figure}[!ht]
  \centering
      \begin{subfigure}{\linewidth}
          \includegraphics[width=\linewidth]{fluxo_ap_circulatorio}
      \caption{Fluxo de pacientes diagnosticados com problema no aparelho circulatório.}
      \end{subfigure}
  \hfil
      \begin{subfigure}{\linewidth}
          \includegraphics[width=\linewidth]{fluxo_gravidez}
      \caption{Fluxo de pacientes com diagnósticos relacionados a gravidez.}
      \end{subfigure}
  \caption{Fluxo de pacientes no estado do Rio de Janeiro em 2019 para dois diagnósticos distintos. Imagem obtida da plataforma desenvolvida neste projeto.}
  \label{fig:fluxo_diagnosticos}
\end{figure}

%% ------------------------------------------------------------------------- %%
\subsection{Malha Municipal e Censo Demográfico}
\label{subsec:malha}
A Malha Municipal\footnote{\url{https://www.ibge.gov.br/geociencias/organizacao-do-territorio/malhas-territoriais/15774-malhas}} disponibilizada anualmente pelo Instituto Brasileiro de Geografia e Estatística (IBGE) contém a representação 
político-administrativa dos estados e municípios brasileiros. Para o projeto, utilizamos a atualização de 2021 com arquivos 
agrupados por estados. Esta base é composta pela representação vetorial de cada município, organizada em arquivos no 
formato \textit{shapefile}, viabilizando a visualização georreferenciada das análises deste estudo. 
Por exemplo, o limite estadual do Rio de Janeiro apresentado na Figura \ref{fig:fluxo_diagnosticos} foi construído com essa base. 
Utilizamos também as estimativas populacionais do Censo Demográfico de 2010, realizado pelo IBGE, com a finalidade de realizar uma análise descritiva inicial da movimentação de pacientes. 
Essa análise será apresentada na Seção \ref{sec:estatisticas}. 

%% ------------------------------------------------------------------------- %%
\subsection{Limites das Regiões e Macrorregiões de Saúde}
\label{subsec:limites_regioes}
Como explicado na Seção \ref{sec:regioes_macro}, as regiões e macrorregiões de saúde são partições territoriais que agrupam municípios vizinhos. 
A base que contém essas informações para as regiões de saúde é estruturada pela relação município e região a qual pertence, para cada ano entre 2011 e 2017. 
Para as macrorregiões de saúde, a estrutura é a mesma, mas temos apenas a atualização de março de 2020. 
Essas informações foram obtidas pelo portal SAGE\footnote{\url{http://sage.saude.gov.br/}} (Sala de Apoio à Gestão Estratégica) de monitoramento do SUS, do Departamento de Monitoramento e Avaliação do SUS (DEMAS). 

%% ------------------------------------------------------------------------- %%
\section{Abordagens de análise}
\label{sec:abordagens}
Como as regiões e macrorregiões de saúde são organizadas por estado da federação, decidimos realizar as análises também por estado. 
Nelas, ao montar a rede de fluxo de pacientes, consideramos inicialmente os municípios como 
vértices e o fluxo de pacientes entre municípios como arestas desta rede. Cada aresta possui um peso relacionado à 
quantidade de pacientes que se movimentaram no sentido desta aresta. Lembrando que, como cada análise considera apenas os 
municípios daquele estado, o fluxo interestadual não é considerado neste nível da análise. 

De qualquer forma, não existem impeditivos para uma futura análise ao nível nacional que considere os fluxos intermunicipais e interestaduais. 
Ela seria interessante para analisar a situação da saúde nas cidades de fronteira estadual, pois 
os habitantes de uma cidade com fronteira para outro estado podem tender a buscar atendimento hospitalar em locais 
mais próximos ou de maior disponibilidade, mesmo que precise atravessar um limite estadual. 

Definiremos a seguir as abordagens aplicadas nesta pesquisa para cumprir com seu objetivo de analisar o processo de 
regionalização do SUS respondendo às três questões de pesquisa elaboradas neste trabalho (vide Seção \ref{sec:motivacao}). 
As Seções \ref{subsec:metodo_comunidades} e \ref{subsec:metodo_indice} abaixo utilizam estratégias 
recomendadas pela literatura. Vimos aplicações destas mesmas estratégias, ou semelhantes, no capítulo anterior em trabalhos 
relacionados apresentados nas Seções \ref{sec:trabalhos_comunidades} e \ref{sec:trabalhos_indices}, respectivamente. 
Além disso, a Seção \ref{subsec:metodo_visualizacao} apresenta uma terceira abordagem, mais visual, de análise do processo 
de regionalização por visualizações em mapa e gráficos. 

\vspace{1em}
%% ------------------------------------------------------------------------- %%
\subsection{Detecção de comunidades}
\label{subsec:metodo_comunidades}
A partir da rede de fluxo intermunicipal de pacientes, podemos aplicar a técnica de detecção de comunidades (vide Seção 
\ref{subsubsec:deteccao_de_comunidades}) para encontrar possíveis agrupamentos de municípios a partir dos dados. Como as 
regiões de saúde são, por definição, agrupamentos de municípios adjacentes pertencentes a um mesmo estado, podemos fazer uma análise de comparação dos agrupamentos 
sugeridos pela solução orientada pelos dados e os agrupamentos definidos na organização original das regiões de saúde.
Essa abordagem responderia à questão de pesquisa $Q1$ (\textit{A movimentação dos pacientes está relacionada à delimitação das regiões de saúde? O quanto?}), já que 
compara o comportamento da movimentação de pacientes, pelos dados, com a delimitação das regiões de saúde. 

Testamos dois algoritmos diferentes para detecção de agrupamentos na rede: o \textit{greedy modularity} (vide Seção \ref{paragraph:greedy_modularity}) 
e o \textit{infomap} (vide Seção \ref{paragraph:infomap}). A aplicação deles foi elaborada com o auxílio dos pacotes \textit{Python}: NetworkX\footnote{\url{https://networkx.org/}} 
e Infomap\footnote{\url{https://mapequation.org/infomap/}}, ambos projetos de código
aberto e em constante evolução. A partir deles já conseguimos identificar alguns resultados relevantes, que serão 
expostos no próximo capítulo na Seção \ref{sec:resultado_comunidades}.

Os dois algoritmos diferem bastante não só em questão de definição e implementação, mas também em sua aplicação. O 
\textit{greedy modularity}, implementado pelo NetworkX, não considera o peso e nem a direção das arestas da rede, prejudicando a precisão de 
nossa análise, já que a direção do fluxo (partir do município A para B, diferente de partir de B para A) e a quantidade de pacientes atrelada a ele são informações de extrema relevância. 
Já o algoritmo \textit{infomap} considera todos esses aspectos da rede e apresenta um resultado mais apropriado para o nosso caso. 
Outra diferença notada é a possibilidade de sugestão, por parte do usuário, de uma quantidade específica de agrupamentos 
a serem detectados. Ambos algoritmos calculam a quantidade e a organização dos agrupamentos, por padrão, mas apenas o 
\textit{infomap} permite que sugiramos uma quantidade específica de agrupamentos a serem detectados. Isso auxilia no 
momento da comparação dos agrupamentos de municípios encontrados com os agrupamentos originais das regiões de saúde, 
pois podemos fixar a quantidade de agrupamentos ideal para a quantidade de regiões de saúde no estado.

Por fim, aplicamos a métrica de similaridade entre partições (vide Seção \ref{subsec:similaridade_particoes}) justamente para obter uma métrica de semelhança entre os agrupamentos encontrados pelo 
algoritmo e os determinados pelas regiões de saúde, para cada ano da base. 
Como possuímos os dados da movimentação de pacientes para as últimas duas décadas, com essa métrica podemos analisar objetivamente 
a semelhança entre as duas partições ao longo dos anos, isto é, o quanto a movimentação dos pacientes tem respeitado os limites regionais. 

%% ------------------------------------------------------------------------- %%
\subsection{Índice de desempenho}
\label{subsec:metodo_indice}
Outra abordagem utilizada no projeto é o cálculo de um índice de desempenho que indique o nível de autossuficiência de uma região 
de saúde a partir do fluxo de pacientes. 
Com essa abordagem responderíamos à questão de pesquisa $Q2$ (\textit{Existe uma evolução na autossuficiência das regiões de saúde ao longo dos anos?}) 
e, com o resultado desse índice e com a informação das alterações nas regiões de saúde ao longo dos anos, também responderíamos 
a $Q3$ (\textit{As alterações das regiões de saúde ao longo dos anos interferiram na movimentação dos pacientes? Houve melhorias?}). 
Por fim, a intenção aqui é ter uma representação espacial que aponte regiões que estejam precisando de um reforço na infraestrutura de saúde, 
conforme o resultado do índice para as regiões. 
Para isso, testamos dois métodos diferentes. O primeiro é a aplicação dos coeficientes de Elzinga e Hogarty (LIFO e LOFI), e 
o segundo são as taxas de permanência e atração, desenvolvidas nesta pesquisa. 

%Neste caso, a rede representa o fluxo de pacientes entre regiões de saúde de um determinado estado brasileiro. Logo, cada vértice aqui representa uma região de saúde, 
%e as arestas contêm a quantidade de pacientes que se movimentam entre duas regiões diferentes para buscar atendimento hospitalar. 
%Para determinar se certa região de saúde é autossuficiente segundo os dois coeficientes, testamos duas maneiras diferentes. 

\paragraph{Coeficientes LIFO e LOFI}
\label{paragraph:lifo_lofi_metodo}
Inicialmente, fixamos uma porcentagem mínima de 75\% para o LIFO e o LOFI (definidos na Seção \ref{subsubsec:coeficientes}). Fizemos essa escolha baseando no que foi definido por Elzinga e Hogarty e por ser 
adotado em outros trabalhos relacionados \citep{morrisey,frech,rigoli2019} que utilizaram essa configuração para estudar 
áreas de mercados hospitalares. Portanto, dessa forma, uma região de saúde seria considerada bem regionalizada somente se ambos coeficientes 
apresentassem valores acima de 0,75. 
Entretanto, essa maneira não se mostrou adequada como índice de desempenho para as regiões, pois o coeficiente LIFO desvalorizava regiões que atendiam muitos pacientes de fora. 
Dessa forma, as regiões que possuíam uma melhor infraestrutura não foram consideradas autossuficientes por este método. A Seção \ref{sec:resultado_indice} apresentará esse resultado com mais detalhes. 

\paragraph{Taxas de permanência e atração}
\label{paragraph:taxas_metodo}
Com base no resultado que tivemos ao testar o método anterior, combinamos os coeficientes LIFO e LOFI modificados de forma a evitar os mesmos erros. 
Para isso, ao invés de utilizar os coeficientes originais, optamos em utilizar o oposto do coeficiente LIFO, i.e. $1-LIFO$, chamando-o de ``taxa de atração'', em combinação com o LOFI, a ``taxa de permanência'', como apresenta a equação abaixo: 
\begin{align*}
    \text{\textbf{Taxa de permanência}}&=\text{LOFI};
    &\\
    \text{\textbf{Taxa de atração}}&=\text{1 - LIFO}.
\end{align*}

Com esse novo método conseguimos obter coeficientes mais concretos e favoráveis a uma definição de desempenho (de autossuficiência) para as regiões: 
ela cresce conforme a taxa de pacientes que permanecem na própria região de saúde para receber atendimento, indicando o nível de cobertura regional, 
e também cresce conforme a taxa de atração de pacientes para a região, indicando uma capacidade de atendimento superior à capacidade local, isto é, ou a região tem uma estrutura de alto nível, ou seus vizinhos têm uma estrutura de saúde mais precária. 

Com isso, decidimos prosseguir a análise com este último método. Já que os valores das taxas variam entre 0 e 1, a combinação delas é feita por meio da média harmônica ponderada (vide Seção \ref{subsec:media_harmonica}), atribuindo pesos diferentes para os coeficientes 
da seguinte maneira: 
\[ \bar{h}(w)=\frac{(w_{1}+w_{2})x_{1}x_{2}}{w_{1}x_{2}+w_{2}x_{1}}, \text{onde } x_{1}=1-LIFO, x_{2}=LOFI, w_{1}=\beta\text{ e } w_{2}=1; 0 \leq \beta \leq 1. \text{ Logo:} \]
\[ \bar{h}(w)=\frac{(\beta + 1)(1-LIFO)LOFI}{(\beta)LOFI+(1-LIFO)}. \]
Dessa forma, o índice agora valoriza regiões que atraem pacientes de fora, por utilizar o oposto do LIFO, a taxa de atração. 
Além disso, ele prioriza o atendimento regional, isto é, prioriza as regiões que atendem seus próprios pacientes pela taxa de permanência, já que o peso do LOFI na fórmula é geralmente maior ($w_{1} \leq w_{2}$, pois $\beta \leq 1$). 
Isso é favorável à definição de região e macrorregião de saúde, que devem garantir à população o atendimento integral, evitando evasões para outras regiões. 
%Além do uso dos coeficientes LIFO e LOFI, pensamos em montar outro método para determinar se certa região de saúde é suficiente. 
%Para cada região de saúde, calculamos a quantidade de atendimentos dentre os pacientes residentes da região. 
%De forma que:
%pacientes recebidos sobe ou atendimentos locais sobe ==> sobe taxa (?)
%pacientes enviados pra fora sobe ou atendimentos locais desce ==> desce taxa (?)
Os resultados para esses métodos serão detalhados na Seção \ref{sec:resultado_indice}. 

\vspace{0.5em}
%% ------------------------------------------------------------------------- %%
\subsection{Visualização do processo de regionalização}
\label{subsec:metodo_visualizacao}
A terceira abordagem promove uma análise visual e interativa que facilita o entendimento geral do processo de regionalização 
da saúde no Brasil, estado por estado. 
Consideramos importante a atenção a técnicas básicas de visualização de dados, como a disposição visual das informações e a escolha correta de gráficos para representar cada resultado, 
bem como a consideração do público ao qual as visualizações são direcionadas, especialmente gestores de saúde e pesquisadores, para que nada esteja fora do contexto e nem sobrecarregue seu entendimento. 
Um dos precursores em visualização de dados, especialmente dados em mapa, foi o cartógrafo Jacques Bertin, que classificou os elementos visuais na representação de dados em seu livro \textit{Semiologia Gráfica} \citep{bertin1983semiology}. 
Seu trabalho, em conjunto com outras fontes, em especial o livro \textit{Storytelling com dados} de Cole Knaflic \citep{knaflic2019storytelling}, contribuíram na construção das visualizações aqui desenvolvidas. 
Os códigos de todos os testes e implementações dos métodos estão disponíveis em um repositório GitLab \footnote{\url{https://gitlab.com/interscity/health/scripts-sus-regionalization}}, contendo também 
os scripts de processamento das bases que geram os resultados para cada estado da federação (na pasta \texttt{``prepare/scripts/''}). 


Com isso, decidimos desenvolver quatro visualizações principais, compostas por mapas e gráficos, para apresentar os resultados encontrados. 
São elas: (i) a visualização da evolução da movimentação dos pacientes; 
(ii) a visualização da comparação entre a partição sugerida pelo algoritmo de detecção de comunidades e a partição real das regiões de saúde, como visto na Seção \ref{subsec:metodo_comunidades}; 
(iii) a apresentação do índice de desempenho proposto por nós no final da Seção \ref{subsec:metodo_indice}; 
e (iv) a visualização da evolução do comportamento dos limites das regiões de saúde. 
Apresentaremos o resultado das visualizações desenvolvidas na Seção \ref{sec:resultado_visualizacao} do próximo capítulo.

%% ------------------------------------------------------------------------- %%
\section{Plataforma Web}
\label{sec:plataforma}
O objetivo final da nossa pesquisa de mestrado inclui o desenvolvimento de uma plataforma de software livre com a finalidade de fornecer as análises aqui realizadas para gestores, 
pesquisadores e público geral. 
Para isso, escolhemos a biblioteca \textit{Python} \textit{Dash}\footnote{\url{https://dash.plotly.com/}} para criação de \textit{dashboards}, 
que permite o desenvolvimento de uma plataforma web para análise e visualização de dados. 
Na parte da estrutura interna da plataforma (o \textit{backend}), as aplicações \textit{Dash} utilizam o \textit{framework Flask} \footnote{\url{http://flask.pocoo.org/}} como servidor web, se comunicando por solicitações HTTP. 
Já na parte de interface web da plataforma (o \textit{frontend}), ela renderiza componentes usando o \textit{React.js} \footnote{\url{https://pt-br.reactjs.org/}}, biblioteca de interface de usuário em \textit{JavaScript}. 

Assim como outras bibliotecas de criação de \textit{dashboards}, a estrutura do \textit{Dash} se assemelha à estrutura \textit{Model-View-Controller} (MVC), descrita pela primeira vez por \cite{krasnerpope}.
Neste caso, o \textit{Model} seria um conjunto de \textit{DataFrames} \textit{Pandas} (biblioteca de manipulação e análise de dados)\footnote{\url{https://pandas.pydata.org/}}, gerenciando o comportamento e estrutura dos dados dentro da plataforma. 
Os Componentes \textit{Dash} representam o \textit{View}, responsável por apresentar as informações ao usuário de forma visual. 
Por último, as funções de \textit{callback} representam os \textit{Controllers}, que têm a responsabilidade de controlar os dados enviados para visualização de acordo com a interação do usuário. 

\begin{figure}[]
    \centering
      \includegraphics[width=0.7\textwidth]{mvc_dashboard}
    \caption{Representação visual da estrutura da plataforma web desenvolvida. Inspirada no modelo Model-View-Controller (MVC), a plataforma replica a estrutura para três seções da plataforma, representadas pelas três cores diferentes.}
    \label{fig:mvc_dashboard}
\end{figure}

A Figura \ref{fig:mvc_dashboard} representa a estrutura geral do \textit{dashboard} desenvolvido. 
Nele, cada uma das três principais seções do dashboard (``Fluxos de pacientes'', ``Divisões sugeridas'', e ``Permanência vs. atração''; detalhadas na Seção \ref{sec:resultado_visualizacao}) contém seu próprio \textit{model}, \textit{view} e \textit{controller}, representados na figura em três diferentes cores. 
Ou seja, cada seção funciona independentemente das demais: utilizam \textit{DataFrames} diferentes, possuem funções de \textit{callback} diferentes, e são visualmente constituídas por componentes diferentes.
Dessa forma, o código da plataforma (disponível em \url{https://gitlab.com/interscity/health/dashboard-sus-regionalization}) separa esses elementos na pasta \texttt{``data/''}, no arquivo \texttt{``app.py''}, e em \texttt{``components/''}, respectivamente. 
Assim, os elementos são organizados possibilitando a criação de outras seções na plataforma, estendendo-a para novas análises. 

Em suma, a iniciativa em desenvolver uma plataforma web, disponibilizando essas análises em um \textit{dashboard}, 
pode incentivar pesquisas sobre o processo de regionalização da saúde no SUS, 
além de promover tomadas de decisão por parte dos gestores de saúde pública. 
No capítulo seguinte, apresentamos os resultados que obtivemos a partir da implementação dos métodos aqui descritos. 
