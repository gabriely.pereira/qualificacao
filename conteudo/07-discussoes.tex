%!TeX root=../TextoGabriely.tex
%("dica" para o editor de texto: este arquivo é parte de um documento maior)
% para saber mais: https://tex.stackexchange.com/q/78101/183146

%% ------------------------------------------------------------------------- %%
\chapter{Discussões Finais}
\label{cap:discussoes}
No capítulo anterior, apresentamos de forma geral os resultados obtidos em nossa pesquisa através das abordagens desenvolvidas. 
Elas são procedimentos que analisaram e avaliaram a eficiência das regiões e macrorregiões de saúde e sua evolução temporal através de técnicas de sistemas complexos. 
Para dar suporte à elaboração de políticas públicas baseadas em evidências, desenvolvemos uma plataforma única e integrada de software livre do tipo \textit{dashboard}, disponibilizando-a publicamente. 
Neste capítulo, discutiremos como os resultados obtidos podem auxiliar gestores ou pesquisadores a analisar e avaliar o processo de regionalização em diferentes partes do Brasil e sob diferentes fatores. 

Para isso, separamos alguns casos de utilização da plataforma para exemplificar seu potencial na prática. 
Para tanto, este capítulo foi organizado em três seções, em referência às três abordagens da metodologia e às três principais abas da plataforma: 
``Fluxo de pacientes'', ``Divisões sugeridas'' e ``Permanência vs. atração''. 


\section{Fluxo de pacientes}
\label{sec:fluxo_pacientes_discussoes}
Na primeira aba, o usuário da plataforma já consegue fazer algumas análises através do processamento e exibição da movimentação de pacientes ao longo dos estados brasileiros. 
A plataforma permite, por exemplo, o processamento das internações exibidas no mapa pelo diagnóstico principal. 
A Figura \ref{fig:fluxos_discussoes_sp} exibe o resultado desse processamento para alguns casos. 

Interessante notar que, na Figura \ref{fig:fluxos_discussoes_sp}(a), que apresenta os resultados para neoplasias\footnote{De acordo com o Instituto Nacional de Câncer (INCA), neoplasia é um tumor que ocorre pelo crescimento anormal do número de células. Quando maligna, considera-se câncer.}, a distância do deslocamento de pacientes é maior (as linhas dos fluxos são mais longas). 
Embora a capital possua uma maior densidade de internações, algumas cidades do interior do estado de São Paulo, como Jaú e Barretos, são destinos bastante procurados por pacientes de diversos pontos do interior do estado. 
Esses dois municípios atendem pacientes externos em maior proporção do que atendem os seus próprios. Isso revela que eles devem ter uma boa estrutura de atendimento hospitalar especializado para neoplasias. 
Entretanto, isso revela também que vários pacientes do interior se deslocam por muitos quilômetros para receber o devido atendimento para seu caso. 

Já os outros diagnósticos possuem comportamento semelhante à movimentação geral de pacientes pelo estado (Figura \ref{fig:fluxos_discussoes_sp}(d)), porém em menor proporção. 
De toda forma, constatamos que existe uma grande distribuição de ``municípios polo'' neste estado, isto é, de municípios que tendem a receber internações dos municípios vizinhos. 
Vemos isso conforme os pontos no mapa onde a ponta do fluxo é laranja, indicando o destino de internações. 

\begin{figure}
    \centering
    \begin{subfigure}{0.48\linewidth}
        \includegraphics[width=\linewidth]{flow_neoplasm_sp}
    \caption{Neoplasias}
    \end{subfigure}
    \hfil
    \begin{subfigure}{0.48\linewidth}
        \includegraphics[width=\linewidth]{flow_circulatory_sp}
    \caption{Doenças do aparelho circulatório}
    \end{subfigure}
    \begin{subfigure}{0.48\linewidth}
        \includegraphics[width=\linewidth]{flow_pregnancy_sp}
    \caption{Gravidez}
    \end{subfigure}
    \hfil
    \begin{subfigure}{0.48\linewidth}
        \includegraphics[width=\linewidth]{flow_total_sp}
    \caption{Todos os diagnósticos}
    \end{subfigure}
    \caption{Fluxos intermunicipais de pacientes no estado de São Paulo em 2019, separado por alguns diagnósticos.}
    \label{fig:fluxos_discussoes_sp}
\end{figure}

\section{Divisões sugeridas}
\label{sec:divisoes_sugeridas_discussoes}
Na segunda aba, o usuário é estimulado a fazer uma análise comparativa entre os limites regionais e a sugestão do algoritmo. 
Com isso, podemos identificar diferentes casos na comparação. Por exemplo, casos em que uma região sugerida é mais ampla que a região original; casos onde o oposto ocorre; além de casos onde o algoritmo sugere a divisão (ou união) de regiões em duas ou mais partes. 
A Figura \ref{fig:expansao_discussoes_rj} apresenta um caso onde a região sugerida expande a região de saúde original. 
Essa é a região que contém a capital do estado, e a sugestão se baseia nas movimentações dos pacientes no ano de 2019. 
A Figura \ref{fig:expansao_discussoes_rj}(b) apresenta em azul a região original, e a região sugerida seria a composição da original com as duas partes vizinhas em amarelo. 
Essa sugestão indica que o vínculo entre as áreas em azul e em amarelo, por meio dos fluxos de pacientes entre elas, foi tão forte que o algoritmo considera como uma única região. 


\begin{figure}
    \centering
    \begin{subfigure}{0.48\linewidth}
        \includegraphics[width=\linewidth]{expansao_rj}
    \caption{}
    \end{subfigure}
    \hfil
    \begin{subfigure}{0.48\linewidth}
        \includegraphics[width=\linewidth]{expansao_zoom_rj}
    \caption{}
    \end{subfigure}
    \caption{Comparação da divisão das Regiões de Saúde do Rio de Janeiro com a divisão detectada pelo algoritmo Infomap através da rede de movimentação de pacientes em 2019.}
    \label{fig:expansao_discussoes_rj}
\end{figure}


Além disso, essa mesma aba do \textit{dashboard} apresenta o nível de semelhança entre a divisão original das regiões e a divisão sugerida. 
Com essa métrica, conseguimos analisar a evolução temporal dessa similaridade, descobrindo momentos irregulares no período estudado. 
A Figura \ref{fig:divisoes_discussoes_rj} apresenta um exemplo no estado do Rio de Janeiro para as Macrorregiões de Saúde. 
Em foco, a figura mostra a comparação entre a divisão original das macrorregiões e a divisão sugerida pelo algoritmo para os dados de movimentação de pacientes em 2011 e 2012. 
Olhando para a evolução temporal da similaridade entre as divisões, encontramos um grande declive em 2011 seguido de um pico em 2012 (marcados pelas linhas pontilhadas na Figura \ref{fig:divisoes_discussoes_rj}(c)). 
Isso revela momentos em que, de acordo com os dados, o comportamento da movimentação dos pacientes, ao nível macrorregional, não se comportou conforme o esperado originalmente. 
Dessa forma, supondo que não tenha havido problemas no registro dos dados naqueles anos, esse resultado pode indicar períodos nos quais houve problemas significativos no serviço de saúde do estado. 

\begin{figure}
    \centering
    \begin{subfigure}{0.48\linewidth}
        \includegraphics[width=\linewidth]{divisoes_2011_rj}
    \caption{Comparação em 2011}
    \end{subfigure}
    \hfil
    \begin{subfigure}{0.48\linewidth}
        \includegraphics[width=\linewidth]{divisoes_2012_rj}
    \caption{Comparação em 2012}
    \end{subfigure}


    \begin{subfigure}{\linewidth}
        \includegraphics[width=\linewidth]{similaridade_discussoes}
    \caption{}
    \end{subfigure}
    \caption{Comparação da divisão original das Macrorregiões de Saúde com as divisões sugeridas para 2011 e 2012 no estado do Rio de Janeiro.}
    \label{fig:divisoes_discussoes_rj}
\end{figure}


\section{Permanência vs. atração}
\label{sec:permanencia_atracao_discussoes}
Por fim, a terceira aba principal disponibiliza ao usuário a análise temporal das taxas de permanência, atração de pacientes, e sua combinação. 
Com isso, ele consegue identificar em cada estado regiões que tiveram melhorias ou pioras no índice, temporalmente. 
Além disso, ele pode identificar regiões que sofreram alterações em seus limites e 
analisar os valores dos índices perante essa mudança, assim como o caso da Figura \ref{fig:campinas_sp}. 


\begin{figure}[t!]
    \centering
    \begin{subfigure}{0.48\linewidth}
        \includegraphics[width=\linewidth]{campinas_2016}
    \caption{2016}
    \end{subfigure}
    \hfil
    \begin{subfigure}{0.48\linewidth}
        \includegraphics[width=\linewidth]{campinas_2017}
    \caption{2017}
    \end{subfigure}

    \begin{subfigure}{0.48\linewidth}
        \includegraphics[width=\linewidth]{campinas_2018}
    \caption{2018}
    \end{subfigure}
    \hfil
    \begin{subfigure}{0.48\linewidth}
        \includegraphics[width=\linewidth]{campinas_2019}
    \caption{2019}
    \end{subfigure}
    \caption{Alteração da taxa de permanência das Regiões de Saúde próximas a Campinas, São Paulo, que sofreram modificações em seus limites administrativos em 2017.}
    \label{fig:campinas_sp}
\end{figure}

Na Figura \ref{fig:campinas_sp}, a área em foco contém as regiões de saúde do estado de São Paulo que são mais próximas do município de Campinas, em vermelho nos mapas. 
Houve uma mudança nos limites regionais dessa área em foco, e ela influenciou a taxa de permanência de pacientes das regiões neste caso. 
Observamos que a região que contém Campinas está sempre em um tom de azul mais escuro, indicando uma maior taxa de permanência de pacientes. 
Entretanto, a região de saúde que surge em 2017 a noroeste do ponto vermelho (região Oeste VII), que pertencia a mesma região de saúde de Campinas em 2016, contém uma taxa de permanência de pacientes bem mais baixa logo após a mudança. 
Isso indica que, embora tenha havido uma alteração na divisão das regiões, os municípios daquela região mais clara em 2017 possivelmente tinham uma relação forte com Campinas, e ainda continuam a enviar pacientes para lá. 
Ademais, observamos uma readaptação dos fluxos de pacientes anos após a alteração, pelo aumento da taxa de permanência na região Oeste VII nos anos posteriores, como revela a Figura \ref{fig:campinas_sp}(c) e (d). 
Neste caso, o \textit{dashboard} permite monitorar o impacto, ao longo dos anos, das mudanças administrativas efetuadas no SUS e a avaliação do quanto elas estão realmente sendo refletidas na prática. 

Além disso, perceber as regiões de menor taxa de permanência neste mapa do \textit{dashboard} 
é como perceber regiões que precisam de mais recursos hospitalares, já que sua população tende a receber atendimento hospitalar fora da região de origem. 


\begin{figure}
    \centering
    \begin{subfigure}{0.3\linewidth}
        \includegraphics[width=\linewidth]{ceara_1999}
    \caption{1999}
    \end{subfigure}
    \hfil
    \begin{subfigure}{0.3\linewidth}
        \includegraphics[width=\linewidth]{ceara_2009}
    \caption{2009}
    \end{subfigure}
    \hfil
    \begin{subfigure}{0.3\linewidth}
        \includegraphics[width=\linewidth]{ceara_2019}
    \caption{2019}
    \end{subfigure}
    \caption{Nível de desempenho das Regiões de Saúde do Ceará para os anos 1999, 2009 e 2019, combinando taxa de permanência e $\beta\times$taxa de atração por média harmônica, onde $\beta = 0,2$.}
    \label{fig:ceara_desempenho}
\end{figure}


Similarmente, a combinação entre as taxas de permanência e atração de pacientes carregam significado semelhante ao analisado anteriormente, indicando um método de avaliação das regiões. 
A Figura \ref{fig:ceara_desempenho} apresenta essa combinação (ponderando as taxas com peso inteiro para permanência e peso 0,2 para atração; vide Seção \ref{subsec:metodo_indice}) para o estado do Ceará entre os anos de 1999, 2009 e 2019. 
Seu resultado apresenta a evolução espaço-temporal do desempenho das regiões. 
Em 1999 apenas as regiões de Fortaleza (no litoral) e de Sobral estão num tom mais azulado, apresentando valores acima de 0,5, refletindo um bom desempenho. 
Em 2009 o resultado se mantém semelhante, mas em 2019 outras regiões se sobressaem. Delas, além de Fortaleza e Sobral, as regiões de Juazeiro do Norte (ao sul do estado) e de Quixadá (ao centro) superam o 0,5, 
e, pela evolução observada no \textit{dashboard}, seus níveis de desempenho têm crescido gradualmente nos últimos anos e tendem a crescer nos anos futuros. 


Estes foram alguns casos de análises que podem servir de base para a proposição e a análise de impacto de políticas públicas de saúde baseadas em evidências. 
Além destas, o usuário possui grande liberdade para analisar outros casos através da plataforma. 
Adicionalmente, a plataforma é de software livre e pode ser estendida no futuro. 
O objetivo principal aqui é promover novas políticas públicas que melhorem o atendimento hospitalar da população brasileira. 
No próximo e último capítulo, discutiremos as contribuições deste projeto na academia e na prática, além de apresentar as limitações encontradas no caminho 
e alguns possíveis trabalhos futuros a partir do que foi desenvolvido aqui. 


%POR NAS DISCUSSÕES FINAIS?
%Por definição, ser capaz de fornecer serviços de saúde para todos seus habitantes é o esperado por uma região de saúde. 
%Da Região de Saúde, espera-se o fornecimento de serviçõs de baixa e parte da média complexidade, e para as Macrorregiões de saúde espera-se os de média e alta complexidade (vide \ref{sec:regioes_macro}). 
