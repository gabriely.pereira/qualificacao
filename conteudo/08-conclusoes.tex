%!TeX root=../TextoGabriely.tex
%("dica" para o editor de texto: este arquivo é parte de um documento maior)
% para saber mais: https://tex.stackexchange.com/q/78101/183146

%% ------------------------------------------------------------------------- %%
\chapter{Conclusões}
\label{cap:conclusoes}
Este trabalho teve o objetivo de desenvolver e disponibilizar ferramentas de análise da regionalização da saúde pública no Brasil 
por meio da movimentação de pacientes por internações hospitalares. 
Embora a análise da regionalização através das internações hospitalares tenha demonstrado resultados bem relevantes, sabemos que diversos fatores influenciam a delimitação das regiões de saúde, como a acessibilidade por meio de transporte, já que questões de locomoção influenciam a movimentação dos pacientes. 
Por exemplo, observamos que a distância média percorrida pelos pacientes, especialmente no nordeste, foi crescendo ao longo das décadas (vide Tabela \ref{tab:internacoes_distancia}). 
Imaginamos que isso seja uma consequência da evolução da malha rodoviária dos estados e da popularização do transporte aéreo. 
Outro fator que pode afetar essa análise é a evolução do nível econômico do país, impactando nas decisões de deslocamento do paciente, por exemplo, o que influenciaria diretamente as análises de regionalização \citep{rocha2022}. 
De toda forma, acreditamos que as análises aqui desenvolvidas servem de parâmetro para descrever o processo de regionalização de forma geral. 

A partir disso, este capítulo expõe objetivamente as conclusões deste trabalho respondendo às questões de pesquisa, mostrando suas consequências na academia e na prática, apresentando algumas limitações e, 
por fim, descrevendo possíveis trabalhos futuros. 
Como guia para alcançarmos o objetivo geral do projeto, desenhamos três questões de pesquisa (vide Seção \ref{sec:motivacao}). Elas foram respondidas no Capítulo \ref{cap:resultados}, 
mas retomamos as respostas aqui de forma mais objetiva:

\textbf{Q1: A movimentação dos pacientes está relacionada à delimitação das regiões de saúde? O quanto?}
A movimentação de pacientes está, sim, relacionada às regiões de saúde. Isso é demonstrado através da abordagem de detecção de comunidades, 
pelo fato das divisões sugeridas pelo algoritmo serem bastante semelhantes às divisões originais das regiões. 
\textit{O quanto} eles estão relacionados foi respondido por meio do nível de similaridade entre as duas formas de dividir a rede de movimentação de pacientes. 
No Rio de Janeiro, por exemplo, o nível de similaridade esteve em crescimento nos últimos anos (como visto na Figura \ref{fig:aba_divisoes}), indicando um crescimento na relação entre a movimentação de pacientes 
e as regiões de saúde do estado ao longo do último quarto de século. 

\textbf{Q2: Existe uma evolução na autossuficiência das regiões de saúde ao longo dos anos?}
Conforme a definição de autossuficiência (ou desempenho) determinada aqui (vide \ref{subsec:metodo_indice}), nem todas as regiões apresentaram uma boa evolução temporal. 
Algumas se mantiveram bem estáveis (isso foi observado no estado de São Paulo, por exemplo) %, algumas apresentaram piora em um período (em São Paulo e no Rio de Janeiro entre 1999 e 2009, por exemplo), 
e outras apresentaram uma boa evolução (como no Rio de Janeiro e em grande parte das regiões do Ceará), de acordo com os dados. Mas, de forma geral, a média dos estados apresenta grande estabilidade temporal no índice de desempenho, com um leve e constante crescimento nos últimos 10 anos da base de dados analisada. 

\textbf{Q3: As alterações das regiões de saúde ao longo dos anos interferiram na movimentação dos pacientes? Houve melhorias?}
Embora os estados estudados de forma particular no Capítulo \ref{cap:resultados} não apresentem grandes alterações na configuração das regiões, 
os poucos casos em que isso ocorreu revelaram interferir na movimentação dos pacientes ao longo dos anos. 
Em alguns casos, percebemos que no ano logo após à modificação parece haver uma piora no índice de desempenho de alguma das regiões envolvidas (como no litoral do Ceará entre 2011 e 2012, e na região próxima de Campinas entre 2016 e 2017 em São Paulo). 
Entretanto, esse desempenho tende a crescer nos anos seguintes, provavelmente devido à readaptação dos fluxos de pacientes em respeito a nova delimitação administrativa das regiões de saúde. 

% ----------------------------------------------------------------------------------
\section{Contribuições à pesquisa e à prática}
\label{sec:contribuicoes_conclusoes}
Os métodos desenvolvidos para cumprir com o objetivo desta pesquisa gerou contribuições na academia, podendo influenciar futuras pesquisas, 
bem como na prática, abrindo horizontes para a análise do processo de regionalização da saúde no Brasil. 
Em resumo, eles estendem o estado da arte ao implementar uma análise comparativa a partir da detecção de comunidades, 
ao criar um índice de desempenho baseado no fluxo de oferta e demanda dos serviços hospitalares, e ao desenvolver uma plataforma web de software livre 
permitindo a análise do processo de regionalização através de nossa metodologia. 

As contribuições para a academia foram: 
(1) explorar uma forma inédita de sugerir divisões regionais a partir do fluxo de pacientes comparando numericamente sua semelhança com a divisão original das regiões; 
(2) aplicar na área de saúde um método recente de detecção de comunidades (\textit{Infomap}) que se revelou bem eficaz neste caso; 
(3) propor nova maneira de avaliar o desempenho das regiões de saúde conforme seus fluxos externos e internos de pacientes, pelas taxas de permanência e atração; e 
(4) desenvolver um leioute simplificado e intuitivo para expor fluxos, limites territoriais e valores absolutos em mapa por meio de um \textit{dashboard online} interativo. 


Na prática, as principais contribuições desta dissertação foram: 
(1) trazer inovação na análise de uma base pública do DATASUS, a base de internações hospitalares (SIHSUS), fornecendo métodos de visualização mais avançados e intuitivos para ela; 
(2) abrir novos horizontes para futuras análises do fluxo de pacientes em outros níveis de atenção além do nível hospitalar, através da utilização de outras bases do DATASUS; 
(3) permitir análises espaço-temporais de cada estado brasileiro para um longo intervalo de dados (mais de vinte anos); e 
(4) colocar em foco o processo de regionalização de saúde no Brasil, um tema não tão estudado atualmente, mas de significativa importância.

% ----------------------------------------------------------------------------------
\section{Limitações}
\label{sec:limitacoes}
A base de internações hospitalares (SIHSUS), fornecida publicamente pelo Departamento de Informática do Sistema Único de Saúde (DATASUS), apresenta alguns problemas de qualidade da informação. 
Alguns estudos prévios \citep{aguiar2013confiabilidade, melo2004qualidade, costa2003recuperaccao, veras1994confiabilidade, bittencourt2008qualidade, desistemasih} apontaram problemas de confiabilidade em alguns campos da base: o endereço de origem do paciente e seu diagnóstico principal. 
Para o endereço de residência do paciente, foi observado o preenchimento do endereço de destino da internação em alguns casos. No caso onde o diagnóstico principal era câncer de mama e de colo de útero entre 2001 e 2002 no estado do Rio de Janeiro, esse erro ocorreu em quase 10\% dos casos \citep{aguiar2013confiabilidade}.
No campo do diagnóstico, observou-se a ausência de emissão do formulário de internação e alteração do diagnóstico real em mais de 40\% das internações por infarto agudo do miocárdio no município do Rio de Janeiro, em 2000 \citep{melo2004qualidade}. 
Além disso, os primeiros anos da base, na década de 1990, parecem ter dados incompletos, o que foi gradualmente melhorando por volta da virada do século. 
Com esses e outros casos, ficamos cientes de que os dados utilizados pela base SIHSUS não cobrem completamente todas as internações, 
mas como a solução está longe do nosso alcance, a utilizamos por ser o que temos disponível. 


Os limites regionais dos estados brasileiros foram obtidos a partir do portal da Sala de apoio à Gestão Estratégica (SAGE). Neste trabalho utilizamos a atualização anual das regiões de saúde entre os anos de 2011 e 2017, e 2019. 
Para as macrorregiões de saúde, tivemos acesso aos dados apenas para o ano de 2019. 
Dessa forma, as análises desenvolvidas utilizaram os limites regionais do ano disponível mais próximo, já que não tivemos acesso aos dados completos. 

% ----------------------------------------------------------------------------------
\section{Trabalhos futuros}
\label{sec:trabalhos_futuros}
Por fim, identificamos alguns trabalhos que podem ser desenvolvidos a partir dos conhecimentos obtidos em nossa pesquisa. Como a plataforma desenvolvida é de software livre, ela pode ser estendida em outras direções por qualquer grupo interessado. 
Logo, sugerimos os seguintes trabalhos futuros, com relação à adição de novos recursos na plataforma ou criação de novas análises a partir dela: 
\begin{itemize}
    \item Divulgação e validação de nossa metodologia e \textit{dashboard} com gestores, pesquisadores, ativistas e elaboradores de políticas públicas de saúde. Tal validação poderá embasar futuros desenvolvimentos das técnicas e software produzidos em nossa pesquisa. 
    \item Implementação da técnica \textit{edge bundling} de visualização de fluxos, que agrupa arestas de fluxos semelhantes gerando uma visualização mais limpa dos padrões da movimentação dos pacientes pelo estado. Este tema será explorado em outra dissertação de mestrado em andamento em nosso grupo de pesquisa. 
    \item Inserção de outras bases de dados do DATASUS, além das internações hospitalares, para análise do processo de regionalização da saúde em outros níveis de complexidade, como atenção básica. 
    \item Adição de novas opções de filtragem dos fluxos além dos diagnósticos, como o tipo de atendimento, nível de complexidade, sexo, raça/cor ou faixa etária do paciente, por exemplo. 
    \item Análise do processo de regionalização da saúde ao nível interestadual, além do intermunicipal abordado aqui, explorando os fluxos de pacientes que ultrapassam os limites estaduais. 
    \item Semelhante à sugestão anterior, mas analisando o nível intrarregional, explorando os fluxos de pacientes entre municípios, bairros, hospitais, entre outros, dentro de uma região de saúde. 
    \item Avaliação sistemática de usabilidade da plataforma, através do método \textit{System Usability Scale} ou outro semelhante. 
    \item Desenvolvimento de visualização da infraestrutura de saúde das regiões, adicionando quantidade de hospitais, leitos, especializações atendidas em cada região. Essas informações podem ser obtidas publicamente pela base de Cadastro Nacional de Estabelecimentos de Saúde (CNES). 
    \item Criação de análises de indiquem locais que necessitem de mais investimento, de acordo com a demanda de pacientes e baixa infraestrutura na região. 
    \item Análise espaço-temporal da expansão da rede de assistência hospitalar nas regiões, como a evolução da oferta de leitos hospitalares, por exemplo. 
    \item Implementação da atualização mensal da base de internações SIHSUS utilizada pela plataforma, adicionando os meses mais recentes disponibilizados pelo sistema de arquivos do DATASUS. 
    \item Análise da regionalização pelo fluxo de pacientes internados por COVID-19. 
    \item Identificação automática do surgimento de novos polos de atração de pacientes ao longo dos anos, a partir da movimentação de pacientes. 
    \item Realização de previsões do comportamento da movimentação dos pacientes frente a futuras alterações nas delimitações de regiões de saúde através de modelos de aprendizado de máquina. Poderia ser feita uma previsão do desempenho das regiões semelhantemente ao realizado por um colega de nosso grupo de pesquisa \citep{aleixopredicting} na previsão de casos de dengue. 
    \item Utilização de um critério de avaliação formal que compare os resultados dos algoritmos de detecção de comunidades e revele objetivamente qual representa melhor o fluxo de pacientes pelo estado. 
\end{itemize}

Esperamos, enfim, que o trabalho desenvolvido nesta pesquisa seja um novo passo em direção à resolução dos problemas do acesso aos serviços de saúde. 
Acreditamos que ele, juntamente com outros trabalhos que possam ser derivados a partir dele, contribua de alguma forma para a efetivação de um sistema de saúde mais regionalizado, 
melhorando os serviços que são oferecidos para a população em nosso país. 
