%!TeX root=../TextoGabriely.tex
%("dica" para o editor de texto: este arquivo é parte de um documento maior)
% para saber mais: https://tex.stackexchange.com/q/78101/183146

%% ------------------------------------------------------------------------- %%
\chapter{Introdução}
\label{cap:introducao}
Com o aumento da geração e armazenamento de dados, houve um grande empenho computacional para acompanhar esse crescimento. 
A extração de informações em grandes conjuntos de dados foi, com isso, tomando um lugar de grande importância. 
As técnicas de sistemas complexos são ferramentas que realizam esse tipo de tarefa. Elas podem extrair informações não obvias de conjuntos de dados extensos, além de detectar padrões neles. 
Aplicações do uso de sistemas complexos podem ser amplamente encontradas em sistemas sociais, biológicos, epidemiológicos, entre outros. 
O uso de sistemas complexos na saúde pública tem sido direcionado para análises como transmissão de doenças e estrutura interorganizacional de sistemas de saúde, por exemplo \citep{luke}. 

O Sistema Único de Saúde (SUS) do Brasil é um dos maiores sistemas de saúde pública do mundo, garantindo acesso integral, universal e gratuito para toda a população do país 
\footnote{\url{https://www.gov.br/saude/pt-br/assuntos/saude-de-a-a-z/s/sus-estrutura-principios-e-como-funciona}}. 
De acordo com o Sistema de Informações de Beneficiários (SIB), gerido pela Agência Nacional de Saúde Suplementar (ANS), a taxa de cobertura de planos de saúde no Brasil é de 25,7\%, pela atualização de agosto de 2022 
\footnote{\url{http://www.ans.gov.br/anstabnet/cgi-bin/dh?dados/tabnet_tx.def}}. 
Dessa forma, praticamente 3/4 da população brasileira depende apenas do SUS. 

Sua organização é baseada numa estrutura de regionalização do atendimento à população. Podemos entender a relevância da regionalização de saúde no Brasil pelo fato da preocupação pela regionalização ter sido anterior à própria criação do SUS \citep{pacto2006}. Conforme o Pacto pela Saúde, 2006: 
\blockquote{"[A regionalização] é a diretriz que orienta o processo de descentralização das ações e serviços de saúde e os processos de negociação e pactuação entre os
gestores. [...] A regionalização objetiva garantir o direto à saúde da população, reduzindo as desigualdades sociais e
territoriais por meio da identificação e reconhecimento das Regiões de Saúde."} 
Dessa forma, define-se uma Região de Saúde como um conjunto de municípios vizinhos escolhidos para integrar a organização, o planejamento e a execução de ações e serviços de saúde \citep{decreto}. 
Idealmente, os habitantes pertencentes a uma determinada região de saúde não precisariam ultrapassar os limites de sua região para receber os serviços de saúde, 
já que a região de saúde tem o objetivo de atender às demandas e interesses locais \citep{pacto2006}. 
%[criar imagem/diagrama que exemplifique os fluxos interregionais] (?)

Pacientes que precisam ser internados por motivos de insuficiência cardíaca, por exemplo, 
idealmente não deveriam sair de sua região de saúde para serem internados. Assim também acontece para partos, insuficiência respiratória e outros procedimentos de média complexidade. %\footnote{As informações de nível de complexidade dos procedimentos foram obtidas pelo SIGTAP (Sistema de Gerenciamento da Tabela de Procedimentos, Medicamentos e OPM do SUS), 
%pelo link \url{http://sigtap.datasus.gov.br/tabela-unificada/app/sec/inicio.jsp}, em 02/2022.}. 
Isso porque as Regiões de Saúde atendem procedimentos de baixa e parte de média complexidade, enquanto as Macrorregiões de saúde atendem média e alta complexidade (vide Capítulo \ref{cap:SUS}). 

Informações sobre internações de pacientes, seu diagnóstico e localização tanto da residência quanto da internação são disponibilizadas publicamente pelo DATASUS (Departamento de Informática do SUS) no SIH/SUS (Sistema de Informações Hospitalares). 
Com isso, podemos desenhar a rede de fluxos de pacientes entre municípios diferentes e analisar a movimentação desses pacientes em relação aos limites das regiões de saúde. 

Analisar o deslocamento dos pacientes pode informar a adequação das regiões de saúde ao princípio de cobrir sua demanda local, mas vale lembrar que outros fatores como problemas de acessibilidade em transporte 
pode influenciar essa análise. De qualquer forma, nossos resultados indicam uma grande correlação entre a delimitação das regiões e a movimentação dos pacientes. 
Essa correlação tem crescido temporalmente, indicando uma maior adequação dos fluxos de pacientes às suas regiões de saúde nos últimos anos. 

Neste trabalho, buscamos analisar o comportamento, bem como a eficiência das regiões de saúde a partir de técnicas de sistemas complexos com base na movimentação de pacientes 
em internações hospitalares no Sistema Único de Saúde. 
Além disso, disponibilizamos os resultados publicamente implementando uma ferramenta de visualização de dados, especialmente voltada a gestores de saúde, incentivando a elaboração de políticas públicas baseadas em evidências. 


%[=contexto]
%
%- Definir aqui o que é o processo de regionalização de saúde e sua relevância. 
%
%- Falar da movimentação de pacientes entre municípios e a 'idelização' de que, de preferencia, 
%não haja fluxos entre regiões de saúde diferentes. (criar imagem/diagrama que exemplifique isso). 
%
%- Apresentar o fluxo de pacientes como uma rede/sistema e sugerir uso de técnicas de sistemas/redes complexos/as 
%para análise do comportameto e eficiencia das regiões de saúde.

% explicar que a regionalização é um dos fatores na movimentação dos pacientes, além de acessibilidade 

\section{Motivação e Objetivos}
\label{sec:motivacao}
Na organização e gerenciamento das regiões de saúde, os gestores possuem liberdade para delimitar as regiões de saúde, escolhendo os municípios a participar da região de acordo com critérios particulares não pré-estabelecidos. 
Com isso, uma ferramenta orientada por dados que auxilie suas análises poderia ser de grande utilidade. 
\cite{mariana} defendem que os problemas do acesso aos serviços só serão resolvidos através da efetivação da regionalização de saúde. 
Para auxiliar no processo de análise da eficiência e evolução das regiões de saúde no Brasil, buscamos responder às seguintes questões de pesquisa em relação às internações hospitalares cobertas pelo SUS: 

\begin{quote}\emph{\textbf{Q1:} A movimentação dos pacientes está relacionada à delimitação das regiões de saúde? O quanto?}\end{quote}
\begin{quote}\emph{\textbf{Q2:} Existe uma evolução na autossuficiência das regiões de saúde ao longo dos anos?}\end{quote}
\begin{quote}\emph{\textbf{Q3:} As alterações das regiões de saúde ao longo dos anos interferiram na movimentação dos pacientes? Houve melhorias?}\end{quote}

Para alcançar esses objetivos, utilizamos métodos de sistemas complexos, analisando a rede de movimentação de pacientes e, com isso, o comportamento da população que utiliza os serviços de saúde em nível hospitalar. 
Para a primeira questão, comparamos os agrupamentos de municípios delimitados pelas regiões de saúde com os agrupamentos de municípios sugeridos por um algoritmo de \textbf{detecção de comunidades}, utilizando os dados do fluxo de pacientes. 
Dessa forma podemos compreender o quanto as regiões de saúde se refletem no fluxo de pacientes. 
%com ela, saberemos que a regionalização afeta a movimentação dos pacientes.
Na segunda questão, implementamos um \textbf{índice de desempenho} que busca medir o nível de autossuficiência das regiões de saúde através desses dados, para permitir uma análise da evolução da autossuficiência das regiões ao longo do tempo. 
Por fim, também implementamos uma \textbf{plataforma de visualização de dados} de modo a fornecer a gestores de saúde e pesquisadores uma ferramenta que possa auxiliar no gerenciamento e análise das regiões de saúde. 
Ao observar o resultado do desempenho das regiões de saúde em anos diferentes, podemos responder à terceira questão. 

\section{Contribuições}
\label{sec:contribuicoes}
O objetivo desta pesquisa é investigar a eficiência das regiões de saúde brasileiras, bem como sua evolução nas últimas décadas, além de desenvolver uma plataforma iterativa que auxilie o acesso aos resultados dessa análise. 
Com base nesse objetivo, acreditamos ter alcançado as seguintes contribuições como parte deste trabalho. 

\textbf{Verificação da relação entre a movimentação de pacientes e a delimitação das regiões de saúde brasileiras.} 
O processo de regionalização da saúde tem o objetivo de descentralizar o planejamento e acesso aos serviços de saúde pela população. 
Dessa forma, pacientes pertencentes a uma região de saúde não precisariam se locomover para outras regiões para conseguir atendimento. 
Nossos resultados verificam a grande relação entre o fluxo de pacientes e a configuração das regiões. 
Também podemos verificar as áreas em que essa relação é mais forte que outras, o que pode indicar uma maior efetivação da regionalização nelas, 
bem como áreas em que a regionalização não está se comportando como planejado. 

\textbf{Avaliação das regiões de saúde com base na movimentação de pacientes.} 
Podemos encontrar na literatura o desenvolvimento de algumas métricas para a eficiência das regiões de saúde. 
Existe esse interesse em analisar a evolução dos efeitos da regionalização no Brasil, mas poucas iniciativas analisaram o fluxo dos pacientes. 
Este trabalho apresenta uma forma de avaliar a eficiência das regiões de saúde por meio do cálculo das taxas de permanência e atração de pacientes conforme a rede de movimentação de pacientes. 

\textbf{Desenvolvimento de um \textit{dashboard} de análise das regiões de saúde.} 
Uma boa decisão requer o acesso a evidências válidas, especialmente as decisões relacionadas à saúde pública, que atinge a tantos. 
Neste trabalho, desenvolvemos uma plataforma disponibilizando nossos resultados da análise às regiões de saúde de forma integrada e facilmente acessível. 
Isso pode incentivar tomadas de decisão baseadas em evidências por parte dos gestores de saúde, a quem a plataforma se dirige. 

\section{Organização do texto}
\label{sec:organizacao}
Os próximos capítulos estão organizados da seguinte maneira: 
o Capítulo \ref{cap:SUS} explica a origem e funcionamento do SUS e define a regionalização da saúde no Brasil. 
O Capítulo \ref{cap:fundamentos} apresenta os conceitos técnicos utilizados em nossa metodologia para analisar a regionalização de saúde. 
O Capítulo \ref{cap:trabalhos} apresenta alguns trabalhos que utilizaram métodos semelhantes aos usados nesta pesquisa. 
O Capítulo \ref{cap:metodologia} apresenta as bases de dados analisadas aqui, os métodos de análise aplicados e o desenvolvimento da plataforma. 
O Capítulo \ref{cap:resultados} contém os resultados obtidos através de cada um dos métodos, apresenta a plataforma final, além de fazer uma descrição estatística da movimentação de pacientes. 
O Capítulo \ref{cap:discussoes} apresenta a utilidade prática, para os gestores, de cada resultado obtido por meio de alguns exemplos. 
E o Capítulo \ref{cap:conclusoes} contém as conclusões deste trabalho, bem como os possíveis trabalhos futuros. 